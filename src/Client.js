/**
 * Copyright (c) 2018, Visual Fire Development  All Rights Reserved
 * Copyrights licensed under the GNU General Public License v3.0.
 * See the accompanying LICENSE file for terms.
 */

const { AkairoClient, CommandHandler, ListenerHandler } = require("discord-akairo");
const CustomHandler = require("./CustomHandler.js");
const path = require("path");
const { token } = require("./Private/Tokens.js");

class Client extends AkairoClient {
  constructor() {
    super({
      ownerID: ["147891648628654082", "112732946774962176"]
    }, {
      disableEveryone: true
    });
    this.defaultPrefix = "ed$";
    this.debug = false;
    this.commandHandler = new CommandHandler(this, {
      directory: path.join(__dirname, "Commands"),
      allowMention: true,
      handleEdits: true,
      prefix: async function(m) {
        if (!this.client.mongo) return this.client.defaultPrefix;
        try {
          if (!m.guild) return this.client.defaultPrefix;
          const data = await this.client.mongo.fetchGuild(m.guild.id);
          m.data = data;
          if (data.settings.prefix == "default") return this.client.defaultPrefix;
          else return data.settings.defaultPrefix;
        } catch (e) {
          console.debug("Prefix Fetch", e);
          return this.client.defaultPrefix;
        }
      }
    });
    this.listenerHandler = new ListenerHandler(this, {
      directory: path.join(__dirname, "Listeners")
    });
    this.listenerHandler.setEmitters({
      commandHandler: this.commandHandler,
      process
    });
    this.customHandler = new CustomHandler(this, {
      directory: path.join(__dirname, "Modules")

    });
    this.commandHandler.useListenerHandler(this.listenerHandler);
    this.commandHandler.loadAll();
    this.listenerHandler.loadAll();
    this.customHandler.loadAll();
    this.errors = {
      nodb: m => { m.channel.send("I was unable to fetch the database right now. Please try again later."); }
    };
    this.colors = {
      orange: 0xFFA500,
      green: 0x92EE8F,
      red: 0xFF9494
    };
    this.hubID = "173146091640848384";
    this.buildVersion = {
      idNum: "v2",
      idName: "Skeletons"
    };
    this.playingS = [
      {
        title: "with your feelings!", type: "PLAYING"
      }, {
        title: "with dogs!", type: "PLAYING"
      }, {
        title: "Simon Says!", type: "PLAYING"
      }, {
        title: "I Spy!", type: "PLAYING"
      }, {
        title: "with chess pieces!", type: "PLAYING"
      }, {
        title: "with Goomig!", type: "PLAYING"
      }, {
        title: "with a rubber duck!", type: "PLAYING"
      }, {
        title: "your complaints!", type: "LISTENING"
      }, {
        title: "your movements!", type: "WATCHING"
      }, {
        title: "as Big Brother!", type: "PLAYING"
      }, {
        title: "Stranger Things!", type: "WATCHING"
      }, {
        title: "Crunchyroll!", type: "WATCHING"
      }, {
        title: "Spotify!", type: "LISTENING"
      }, {
        title: "your commands!", type: "LISTENING"
      }, {
        title: "your demands!", type: "LISTENING"
      }, {
        title: "FM Radio!", type: "LISTENING"
      }, {
        title: "nervous users!", type: "LISTENING"
      }, {
        title: "users nervously!", type: "LISTENING"
      }, {
        title: "e$help || e:help", type: "PLAYING"
      }, {
        title: "my gears creak!", type: "LISTENING"
      }, {
        title: "with my database!", type: "PLAYING"
      }, {
        title: "my database!", type: "LISTENING"
      }, {
        title: "the database!", type: "WATCHING"
      }
    ];
    this.helpMsgs = [
      /* Goomig */
      "Help is flying through the sky, into your DMs!", "Help is sliding on ice, into your DMs!", "Whoops, Slip, SQUASH THAT REQUEST FOR HELP HERE I COME!",
      /* Dwigoric */
      "Sending 911 under your doormat!", "Delivering the savory pizza in front of your door!", "Sending angels into your mailbox!", "Hooman, I have heeded your request for help.", "So help me God", "Helping you get out of your problems...", "Magic stones, ye say? Right here under the kilt.", "Helping you escape the shadow realm...", "Let me check that I have this right...", "I am really sorry to hear that. Is there anything I can do to do for your request for help?", "That is a good question. Find it out yourself.", "I'm not sure, but don't ask me for help.", "I'm sorry, I don't have the information on that. May I put you on hold for a few minutes? I will clarify this with our manager.", "I'm sorry, this question would be out of my expertise, but Daniel from the tech support department will be able to help you. Would you like me to connect you with him?", "May I put your call on hold while I am checking your order?", "I'm sorry, Echo is not available right now. May I help you with something?", "I'm sorry, we don't have this feature at the moment. We do intend to add it to our service and we can notify you when it has been done. Would you like to receive an email update?",
      /* Wistful */
      "YA NEED HELP?",
      /* Fiery_Hacker */
      "Bringing in the reinforcements..."
    ];
  }
  get mongo() {
    return this.customHandler.modules.get("mongodb");
  }
  log() {
    return console.log.apply(console.log, [`[Client]`].concat(...arguments));
  }
  debug() {
    return console.debug.apply(console.log, [`[Client]`].concat(...arguments));
  }
  error() {
    return console.error.apply(console.error, [`[Client]`].concat(...arguments));
  }
}

const client = new Client();

console.log_orig = console.log;
console.error_orig = console.error;
console.log = function() { return console.log_orig.apply(console.log, [`[Shard ${client.options.shardId}]`].concat(...arguments)); };
console.debug = function() { if (client.debug) return console.log.apply(console.log, [`[Debug]`].concat(...arguments)); };
console.error = function() { return console.error_orig.apply(console.error, [`[Shard ${client.options.shardId}]`].concat(...arguments)); };

client.flup = function(text, m) {
  return text.substring(0, 1).toUpperCase() + text.substring(1, m.content.length);
};

client.getRole = function(type, v, g) {
  if (type == "id") {
    return g.roles.get(v);
  } else if (type == "name") {
    return g.roles.find(r => r.name.toLowerCase() === v.toLowerCase());
  }
};

setInterval(() => {
  const cS = client.playingS[Math.floor(Math.random() * client.playingS.length)];

  client.user.setActivity(cS.title, {
    type: cS.type
  });
}, 60000);

client.log("Logging in...");
client.login(token);
client.mongo.connect().catch(console.error);
