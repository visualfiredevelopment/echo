/**
 * Copyright (c) 2018, Visual Fire Development  All Rights Reserved
 * Copyrights licensed under the VFD Contribution Only License.
 * See the accompanying LICENSE file for terms.
 */

/* eslint no-unused-vars: 0 */

const akairo = require("discord-akairo");
const { Command } = require("discord-akairo");
const discord = require("discord.js");
const prettyms = require("pretty-ms");

class Eval extends Command {
  constructor() {
    super("eval", {
      aliases: ["eval", "evaluate"],
      ownerOnly: true,
      typing: true,
      args: [
        {
          id: "content",
          match: "content"
        }
      ]
    });
  }
  async exec(m, args) {
    try {
      await m.channel.send(`\`\`\`js\n${eval(args.content)}\n\`\`\``);
    } catch (e) {
      await m.channel.send(`\`\`\`js\n${e}\n\`\`\``);
    }
  }
}

module.exports = Eval;