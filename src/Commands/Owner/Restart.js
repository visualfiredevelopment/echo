const { Command } = require("discord-akairo");
const { exec } = require("child_process");

class Restart extends Command {
  constructor() {
    super("restart", {
      aliases: ["restart"],
      ownerOnly: true,
      typing: true,
      args: [
        {
          id: "content",
          type: "lowercase"
        }
      ]
    });
  }
  async exec(m, args) {
    if (!args.content) return m.channel.send("Sorry, you need to supply a `shard id` or `all` to successfully restart the bot.");
    if (args.content == "all") { 
      m.channel.send("What is the `Name/ID` of the process you want to restart?").then(() => {
        m.channel.awaitMessages(msg => m.author.id === msg.author.id, {
          max: 1,
          time: 30000,
          errors: ["time"]
        }).then(collected => {
          if (!collected.size) return m.channel.send("Cancelling restart request.");
          if (collected.first().content.toLowerCase() == "request_list") {
            exec("pm2 list", (err, r) => {
              return m.channel.send(`\`\`\`js\n${r}\`\`\``);
            });
          } else {
            const ptr = collected.first().content;
            m.channel.send("Restarting the Whole Bot!").then(() => {
              try {
                exec(`pm2 restart ${ptr}`);
              } catch (e) {
                m.channel.send(`There was an issue restarting the bot. \`\`\`js\n${e}\n\`\`\``);
              };
            });
          }
        });
      });
    } else {
      await m.channel.send(`Attempting to restart shard ${args.content}.`);
      await this.client.shard.broadcastEval(`if (this.shard.id == ${args.content.toString()}) process.exit();`);
    }
  }
}

module.exports = Restart;